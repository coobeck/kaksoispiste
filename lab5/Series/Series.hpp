#include <iostream>

class Series {
    char *name;
    protected:
        std::size_t noOfPoints;
    public:
        void set_name(char *name);
        char *get_name();
        friend std::ostream& operator<<(std::ostream &os, const Series &Ss);
        Series();
        ~Series();
        Series(std::size_t noOfPoints, const char *name);
};
