#include "Series.hpp"
#include <cstring>
#include <cstdio>

void Series::set_name(char *name) {
    std::strcpy(Series::name, name);
};

char *Series::get_name() {
    return Series::name;
};

std::ostream& operator<<(std::ostream &os, const Series &Ss) {
    printf("%lu\n%s\n", Ss.noOfPoints, Ss.name);
    return os;
};

Series::Series() : Series::Series(14, "mala_seria") {};

Series(std::size_t noOfPoints, char *name) {
    std::stcpy(Series::name, name);
    Series::noOfPoints = noOfPoints;
}
