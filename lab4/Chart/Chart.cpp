#include "Chart.hpp"

void Chart::set_name(const char *name) {
    Chart::name = name;
}

const char *Chart::get_name() {return name;}

void Chart::draw() {std::printf("Nazwa tego obiektu klasy Chart to: %s\n",
        get_name());
};
