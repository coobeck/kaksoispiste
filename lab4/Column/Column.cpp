#include "Column.hpp"

void Column::printname() {std::printf("Nazwa tego obiektu klasy Column to: %s\n",
        Column::get_name());}

void Column::draw() {
    std::printf("#  #  \n"
            "#### #\n"
            "#### #\n"
            "#### #\n"
            "######\n"
            "######\n"
            "######\n");
}

void Column::printchart() {
    Column::printname();
    Column::draw();
}

