#include "../Axe/Axe.hpp"
#include <vector>
#include <iostream>

class AxeSet {
    std::vector<Axe> axe_array;

    public:

    AxeSet(AxeSet &&og);

    ~AxeSet();

    AxeSet &operator=(AxeSet &&axes);

    friend std::ostream &operator<<(std::ostream &os, AxeSet &axes);
};
