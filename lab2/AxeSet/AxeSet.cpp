

#include "AxeSet.hpp"

AxeSet::AxeSet(AxeSet&& og)
    : axe_array{ og.axe_array }
{
    og.axe_array.clear();
}

AxeSet::~AxeSet() {
    AxeSet::axe_array.clear();
}

AxeSet &AxeSet::operator=(AxeSet &&axes) {
    std::cout << axes;
    axe_array = axes.axe_array;
    std::cout << "Przepisano";
    std::cout << axes;
    return axes;
};

std::ostream& operator<<(std::ostream& os, AxeSet& axes) {
    for (std::size_t i = 0; i < axes.axe_array.size(); ++i) {
        std::cout << axes.axe_array[i];
    }
    return os;
};
