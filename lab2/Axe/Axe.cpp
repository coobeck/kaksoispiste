#include "Axe.hpp"

void Axe::set_min(float value) {
    min = value;
}

float Axe::get_min() {
    return min;
}

void Axe::set_max(float value) {
    max = value;
}

float Axe::get_max() {
    return max;
}

void Axe::set_interval(float value) {
    interval = value;
}

float Axe::get_interval() {
    return interval;
}

Axe Axe::add_axes(Axe second) {
    float new_min = std::min(Axe::get_min(), second.get_min());
    float new_max = std::max(Axe::get_max(), second.get_max());
    Axe new_axe(new_min, new_max);
    std::cout << new_axe;
    return new_axe;
}

std::ostream& operator<<(std::ostream& os, Axe& axis) {
    os << "Min " << axis.get_min() << "\nMax " << axis.get_max() << "\nInterval " << axis.get_interval();
    return os;
}

Axe& Axe::operator=(Axe &axis) {
    std::cout << axis;
    min = axis.min;
    max = axis.max;
    interval = axis.interval;
    std::cout << axis;
    return axis;
};

Axe::Axe() {
    Axe::set_min(0);
    Axe::set_max(0);
    Axe::set_interval((max-min)/10);
    ++count;
    std::printf("Użyto konstruktora bezargumentowego, ilosc: %d", Axe::count);
};

Axe::Axe(float min, float max) {
    Axe::set_min(min);
    Axe::set_max(max);
    Axe::set_interval((max-min)/10);
    ++count;
    std::printf("Użyto konstruktora dwuargumentowego, ilosc %d", Axe::count);
};

Axe::~Axe() {
    --count;
    std::printf("Użyto destruktora, ilosc %d", Axe::count);
};
