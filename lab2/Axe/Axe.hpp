#include <iostream>

class Axe {
        float min, max, interval;
        static unsigned int count;

    public:
        void set_min(float);
        float get_min();

        void set_max(float);
        float get_max();

        void set_interval(float);
        float get_interval();

        Axe add_axes(Axe second);

        friend std::ostream &operator<<(std::ostream &os, Axe &axis);

        Axe &operator=(Axe &axis);

        Axe();

        Axe(float min, float max);

        ~Axe();
};
