#include <cstdio>
#include <cstdlib>
#include <vector>

enum class Prefix { // enumerate all usable prefixes
  tera = 12,
  giga = 9,
  mega = 6,
  kilo = 3,
  none = 0,
  mili = -3,
  mikro = -6,
  nano = -9,
  piko = -12,
};

enum class Scale { lin, log };

enum class Unit { V, dB, s, Hz };

class Axis {
  double min, max, interval;
  Prefix pref;

  void update_interval();
  void set_prefix(Prefix pref);

public:
  void set_min(double val);
  void set_max(double val);

  double get_min();
  double get_max();
  Prefix get_pref();
};

class XAxis : Axis {
  Unit si_uni = Unit::Hz;

public:
  void reval(Prefix new_pref);
};

class YAxis : Axis {
  Unit si_uni;
  Scale axis_scale;

public:
  void reval(Prefix new_pref, Unit new_unit);
};

class DataSeries {
  Unit si_uni;
  Prefix get_pref();

  std::size_t no_entries;
  std::vector<double> x_data;
  std::vector<double> y_data;

  YAxis fit_axis(Prefix new_pref, Unit new_unit);

public:
  std::size_t read(const char *pathname);
  double get_max();
  double get_min();
  void reval(Prefix new_pref, Unit new_unit);
};
