\babel@toc {polish}{}
\contentsline {section}{\numberline {1}Pomocnicze typy wyliczeniowe}{2}{section.1}%
\contentsline {subsection}{\numberline {1.1}Typ \emph {Prefix}}{2}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Typ \emph {Scale}}{3}{subsection.1.2}%
\contentsline {subsection}{\numberline {1.3}Typ \emph {Unit}}{3}{subsection.1.3}%
\contentsline {section}{\numberline {2}Klasy osi}{4}{section.2}%
\contentsline {subsection}{\numberline {2.1}Klasa \emph {Axis}}{4}{subsection.2.1}%
\contentsline {subsubsection}{\numberline {2.1.1}Zmienne}{4}{subsubsection.2.1.1}%
\contentsline {subsubsection}{\numberline {2.1.2}Metody}{4}{subsubsection.2.1.2}%
\contentsline {subsection}{\numberline {2.2}Klasy \emph {XAxis} i \emph {YAxis}}{5}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}Zmienne}{5}{subsubsection.2.2.1}%
\contentsline {subsubsection}{\numberline {2.2.2}Metody}{5}{subsubsection.2.2.2}%
\contentsline {subsubsection}{\numberline {2.2.3}Klasa \emph {YAxis}}{5}{subsubsection.2.2.3}%
\contentsline {subsection}{\numberline {2.3}Klasa \emph {DataSeries}}{5}{subsection.2.3}%
\contentsline {subsubsection}{\numberline {2.3.1}Zmienne}{6}{subsubsection.2.3.1}%
\contentsline {subsubsection}{\numberline {2.3.2}Metody}{6}{subsubsection.2.3.2}%
\contentsline {section}{\numberline {3}Wnioski}{7}{section.3}%
