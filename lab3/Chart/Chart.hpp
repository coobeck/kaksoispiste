#include "../../lab2/AxeSet/AxeSet.hpp"

class Chart : public AxeSet {
    const char *name;

    public:

    const char *get_name();

    void set_name(const char *name);

    enum type {XY, Column, Sunburst, Treemap};


};
