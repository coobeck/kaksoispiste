

#include "AxeSet.hpp"

AxeSet::AxeSet(AxeSet&& og)
    : axe_array{ og.axe_array }
{
    og.axe_array.clear();
}

AxeSet::~AxeSet() {
    AxeSet::axe_array.clear();
}
