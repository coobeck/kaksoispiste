#include "../Axe/Axe.hpp"
#include <vector>

class AxeSet {
    std::vector<Axe> axe_array;

    public:

    AxeSet(AxeSet &&og);

    ~AxeSet();
};
