#include <cstdio>

#include "Axe.hpp"

void Axe::set_min(float value) {
    min = value;
}

float Axe::get_min() {
    return min;
}

void Axe::set_max(float value) {
    max = value;
}

float Axe::get_max() {
    return max;
}

void Axe::set_interval(float value) {
    interval = value;
}

float Axe::get_interval() {
    return interval;
}

void Axe::print_vals() {
    std::printf("Min %f,\nMax %f,\nInterval %f.\n", Axe::get_min(), Axe::get_max(), Axe::get_interval());
};

void Axe::print_count() {
    std::printf("Count %u", Axe::count);
}

Axe::Axe() {
    Axe::set_min(0);
    Axe::set_max(0);
    Axe::set_interval((max-min)/10);
    ++count;
    std::printf("Użyto konstruktora bezargumentowego, ilosc: %d", Axe::count);
};

Axe::Axe(float min, float max) {
    Axe::set_min(min);
    Axe::set_max(max);
    Axe::set_interval((max-min)/10);
    ++count;
    std::printf("Użyto konstruktora dwuargumentowego, ilosc %d", Axe::count);
};

Axe::~Axe() {
    --count;
    std::printf("Użyto destruktora, ilosc %d", Axe::count);
};
