class Axe {
        float min, max, interval;
        static unsigned int count;

    public:
        void set_min(float);
        float get_min();

        void set_max(float);
        float get_max();

        void set_interval(float);
        float get_interval();

        void print_vals();

        static void print_count();

        Axe();

        Axe(float min, float max);

        ~Axe();
};
